<?php
/*
*     Author: Ravinder Mann
*     Email: ravi@codiator.com
*     Web: http://www.codiator.com
*     Release: 1.6
*
* Please direct bug reports,suggestions or feedback to :
* http://www.codiator.com/contact/
*
* Real estate made easy is a commercial software. Any distribution is strictly prohibited.
*
*/

/*
 * Filters data through the functions registered for "viewFullListing" hook.
 * Passes an array of aruguments. $vargs[0] is the entire html listing data.
 * $vargs[1] is the entire row of data fetched from database for a particular listing id
 */
 

$vargs[0]=viewFullListing($viewListingRow,$mem_id,$showMoreListings);
$vargs[1]=$viewListingRow;

$vdata=call_plugin("viewFullListing",$vargs);
print $vdata[0];

function viewFullListing($viewListingRow,$mem_id,$showMoreListings=""){
    global $ptype, $reid;
    include("config.php");
ob_start();   
$row=$viewListingRow;
if($row['cats']=="ok") $catClause="<div title='".$relanguage_tags["Cats are allowed"]."'><img src='images/animals-cat-ok.png' /></div>";
if($row['cats']=="notok") $catClause="<div title='".$relanguage_tags["Cats are not allowed"]."'><img  src='images/animals-cat-no.png' /></div>";
if($row['dogs']=="ok") $dogClause="<div title='".$relanguage_tags["Dogs are allowed"]."'><img src='images/animals-dog-ok.png' /></div>";
if($row['dogs']=="notok") $dogClause="<div title='".$relanguage_tags["Dogs are not allowed"]."'><img src='images/animals-dog-no.png' /></div>";
if($row['smoking']=="notok") $smokingClause="<div title='".$relanguage_tags["Smoking is not allowed"]."'><img src='images/no-smoking.png' /></div>";
$reAllowedThings=$catClause.$dogClause.$smokingClause;

if($row['user_id']!="oodle"){
$rePicArray=explode("::",$row['pictures']);
$totalRePics=sizeof($rePicArray);
if ($totalRePics > $reMaxPictures) $totalRePics = $reMaxPictures;
if($row['show_image']=="yes"){
	$qr1="select photo from $rememberTable where id='".$row['user_id']."'";
	$result1=mysql_query($qr1);
	$row1=mysql_fetch_assoc($result1);
	$poster_pic=$row1['photo'];	
}
if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true){
$contactImageClause=" style='float:left;' ";
}
if(trim($poster_pic)!="") $contact_image="<div class='recontact_image' $contactImageClause><img src='uploads/".$poster_pic."' height='125' alt='' /></div>";
else $contact_image="<div class='recontact_image' $contactImageClause><img src='images/identity.png' height='128' alt='' /></div>";
}else{
	$rePicArray=explode("::",$row['pictures']);
	$totalRePics=sizeof($rePicArray);
	if ($totalRePics > $reMaxPictures) $totalRePics = $reMaxPictures;
	$poster_pic=$row['photo'];
	if(trim($poster_pic)!="") $contact_image="<div class='recontact_image'><img src='".$poster_pic."' height='125' alt='' /></div>";
	else $contact_image="<div class='recontact_image'><img src='images/identity.png' height='128' alt='' /></div>";
	
	require_once('geoplugin.class.php');
	$geoplugin = new geoPlugin();
	$geoplugin->locate();
	$vCountry=$geoplugin->countryName;
	$defaultCurrency=getOodleCurrency($vCountry,$defaultCurrency);
}

if($row['bedrooms']=="1den") $row['bedrooms']="1 + ".$relanguage_tags["den"];
if($row['bedrooms']=="2den") $row['bedrooms']="2 + ".$relanguage_tags["den"];

if($row['relistingby']=="owner") $row['relistingby']=$relanguage_tags["Individual"];
if($row['relistingby']=="reagent") $row['relistingby']=$relanguage_tags["Real Estate Agent"];
if($row['price']==0)$row['price']="";
if($row['resize']==0)$row['resize']="";
list($justDate,$justTime)=explode(" ",$row['dttm_modified']);
$newDateFormat=explode("-",$justDate);
$row['dttm_modified']=$newDateFormat[1]."/".$newDateFormat[2]."/".$newDateFormat[0];

    $full_address="";
    if($row['apt']!="") $full_address=$row['apt'];
    if($row['address']!="") $full_address=$full_address.", ".$row['address'];
    if($row['city']!="") $full_address=$full_address.", ".$row['city'];
    if($row['state']!="") $full_address=$full_address.", ".$row['state'];
    if($row['postal']!="") $full_address=$full_address.", ".$row['postal'];
    if($row['country']!="") $full_address=$full_address.", ".$row['country'];
    $full_address=trim($full_address,',');
    $partialAddress=$row['city'].", ".$row['postal'];
    $partialAddress=trim(trim($partialAddress),',');
    if($row['price']!="" && $currency_before_price) $full_price=$defaultCurrency.number_format($row['price'])." - "; else $full_price="";
?>

<div id='rememberAction'>
<table id='resultTable'>
<tr class='headRow1'><td colspan='2' class='bg-primary'><span class='listingTitle'><?php print $full_price.$row['subtype']." - ".$partialAddress; ?></span>
<?php if($row['listing_type']==2){ print "<span class='featuredlisting label label-primary'>".$relanguage_tags["Featured"]."</span>"; } print hasVisited($row['id']); ?>
<div class="pull-right" id='closeMapListing' style="cursor:pointer;"><img src='images/fancy_close.png' height='20' alt='' /></div>
</td></tr>
<?php
$tdbimgWidth="60%";
$buttonsStyle="";
if($totalRePics<=1){
     $tdbimgWidth="70%";
     $buttonsStyle=" style='margin:0;' ";
}
if($totalRePics<=5 && $totalRePics>1) $tdbimgWidth="85%";
if($totalRePics<=10 && $totalRePics>5) $tdbimgWidth="70%";

?>
<tr>
<td style="vertical-align:top; width:<?php print $tdbimgWidth; ?>;" id='image_cell'>
<div id='listingImage'>
<?php if(trim($rePicArray[0])!=""){ ?>
<a data-fancybox-group='listgallery' href='<?php print $rePicArray[0]; ?>' ><img alt='listing image' src='<?php print $rePicArray[0]; ?>' style='width:100%;' /></a>
<?php } ?>
    </div>
</td>
<?php if(isset($_SESSION["winwidth"]) && $_SESSION["winwidth"]<=1170) print "</tr><tr>"; ?>
<td style="vertical-align:top;">
<?php 
$_SESSION["reid"]=$row['id'].":".$_SESSION["reid"];
if($totalRePics>1) { ?>
<div id='listingImages'>
<?php 
$imgRowCount=0;
for($imgCount=0;$imgCount<$totalRePics;$imgCount++){ 
if(trim($rePicArray[$imgCount])!=""){
print "<span id='image_icon-$imgCount'><a href='#'><img src='timthumb.php?src=$rePicArray[$imgCount]&amp;w=200' alt='listing image' /></a></span>";	
}
}
print "<div style='display:none;'>";
for($imgCount=0;$imgCount<$totalRePics;$imgCount++){ 
if(trim($rePicArray[$imgCount])!="") print "<span id='bimage-$imgCount'>$rePicArray[$imgCount]</span>";
}
print "</div>";
?>
</div>
<?php }
$allMarkedreid=explode(":",$_SESSION["marked_reid"]);
$_SESSION['currency_before_price']=$currency_before_price;

function printAttribute($attribute,$tag,$defaultCurrency=""){
   include_once("functions.inc.php");   
   if($attribute!=""){
       if($_SESSION['currency_before_price']) $full_attribute=$defaultCurrency.__($attribute);
       else  $full_attribute=__($attribute)." ".$defaultCurrency;
       if(isset($_SESSION['rtl']) && $_SESSION['rtl']==true) $attrClause=" style='float:left;' ";
     if($tag=="Size") print "<tr><td><b>".__($tag)." (".__("sq-ft")."):</b> <span class='attr_value'>".$full_attribute."</span></td></tr>";
     else print "<tr><td><b>".__($tag).":</b> <span class='attr_value' $attrClause>".$full_attribute."</span></td></tr>";  
   } 
}
?>

<div id='listingButtons' <?php print $buttonsStyle; ?> >
<?php if(in_array($row['id'],$allMarkedreid)){ ?>
<div id='reAlreadyMarkedListing'><span class='btn btn-primary' disabled="disabled" title='<?php print $relanguage_tags["This listing has been liked by you"];?>.'><?php print $relanguage_tags["Liked"]." ".$relanguage_tags["Listing"];?></span></div>
<?php }else{ ?>
<div id='reMarkedListing'><span class='btn btn-default' title='<?php print $relanguage_tags["Mark this listing to find it easily in future"];?>.'  onclick="infoResults('<?php print $row['id']; ?>',8,'reMarkedListing');"><?php print $relanguage_tags["Like Listing"];?></span></div>
<?php } ?>
<a title='<?php print $relanguage_tags["Click above to send a message to the poster of this listing"]; ?>' href='contactPoster.php?reid=<?php print $row['id']; ?>' class='btn btn-default listingcontact'><?php print __("Contact");?></a>
<?php
$ip=$_SERVER["REMOTE_ADDR"];
$listing_id=$row['id'];
$fqr="select * from flagging where ip='$ip' and listing_id='$listing_id'";
$fresult=mysql_query($fqr); 
if(mysql_num_rows($fresult) > 0){
    $reportButton=__("Reported");
    $reportClause=' disabled="disabled" ';
}else{
    $reportButton=__("Report this");
    $reportClause="";
}

?>
<div id='reFlaggedListing'><span onclick="infoResults('<?php print $row['id']; ?>',27,'reFlaggedListing');" class="btn btn-danger listingflag" title="" data-original-title="<?php print __("Flag this listing"); ?>" <?php print $reportClause; ?> ><?php print $reportButton; ?></span></div>

</div>
</td>
</tr>
<?php
if($reAllowedThings!=""){
    $thingsClass=" col-md-1 col-lg-1 ";
    $infoClass=" col-md-4 col-lg-4 ";
    $descClass=" col-md-7 col-lg-7 ";
}else{
    $thingsClass="";
    $infoClass=" col-md-4 col-lg-4 ";
    $descClass=" col-md-8 col-lg-8 ";
}
?>
<tr id='reDescriptionRow'>
<td colspan='2'><div id='listingAllowedThings' class='<?php print $thingsClass; ?>'><?php print $reAllowedThings; ?></div>
<div class='listingItem'><h4><?php print $row['headline']; ?></h4></div>
<div class='listingItem'>
<?php if(isset($_SESSION["winwidth"]) && $_SESSION["winwidth"]>=400) $infoBoxStyle=' style="float:right;" '; else $infoBoxStyle=' style="float:none; width:90%;" '; ?>    
<div class='reAttributes alert alert-info <?php print $infoClass; ?>' <?php print $infoBoxStyle; ?> >
<table id='listing_attributes'>
    
    <tr><td><h4><?php print $full_address; ?></h4></td></tr>
<?php
printAttribute($row['classification'],"Classification");
printAttribute($row['retype'],"Type");
printAttribute($row['subtype'],"Style");
printAttribute(number_format($row['price']),"Price",$defaultCurrency);
printAttribute($row['bedrooms'],"Bedroom");
printAttribute($row['bathrooms'],"Bathroom");
printAttribute($row['resize'],"Size");
printAttribute($row['builtin'],"Built in");
printAttribute($row['relistingby'],"Listing by");
printAttribute($row['dttm_modified'],"Date Listed");
?>
</table>
</div>
    <b><?php print $relanguage_tags["Description"];?>:</b><br /><div class='reListingDescription <?php print $descClass; ?>'><?php print nl2br($row['description']); ?></div>
</div>

<?php if($row['user_id']=="oodle") print "<br /><br /><b>".__("More Information").":</b> <a href='".$row['url']."' target='_blank'>".__("Here")."</a>"; ?>
<?php if($ptype=="viewFullListing"){ ?>
<div class='listingItem' style="clear:both;"><b><?php print $relanguage_tags["Location on map"];?>:</b></div>
<div id='reListingOnMap'></div>
<?php } ?>

<?php if($row['contact_name']!="" || $row['contact_phone']!="" || $row['contact_email']!="" || $row['contact_website']!="" || $row['contact_address']!=""){ ?>
<div class='reContactInformation alert alert-warning' style="clear:both;">
<b><?php print $relanguage_tags["Contact Information"];?></b><?php print $contact_image; ?>
<div class='recontact_info'><br />
<?php if($row['contact_name']!=""){ ?><b><?php print $relanguage_tags["Name"];?>:</b> <?php print $row['contact_name']."<br />"; } ?>
<?php if($row['contact_phone']!=""){ print "<b>".$relanguage_tags["Phone"]." :</b> ".$row['contact_phone']."<br />"; } ?>
<?php if($readmin_settings['listingemail']=="yes"){ print"<b>".$relanguage_tags["Email"].":</b> ".$row['contact_email']."<br />"; } ?>
<?php if($row['contact_website']!=""){ print "<b>".$relanguage_tags["Website"].":</b> <a href='".$row['contact_website']."' target='_blank'>".$row['contact_website']."</a><br />"; } ?>
<?php if($row['contact_address']!=""){ print "<b>".$relanguage_tags["Address"]."</b><br />".nl2br($row['contact_address']); } ?>
</div>
</div>
<?php } ?>
<br /><div class='listingButtons'><?php 
if(isset($_SESSION["memtype"]) && trim($ppemail)!="" && $featuredduration>0 && $featuredprice>0 && $row['listing_type']!=2) featuredButton($row['user_id'],$mem_id,$row['id']); 
showMemberNavigation($row['user_id'],$mem_id,$row['id'],2); 
if(isset($_SESSION["memtype"]) && isset($_SESSION["memtype"]) && trim($ppemail)!="" && $featuredduration>0 && $featuredprice>0  && $row['listing_type']!=2){
?>
<img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
</form>
<?php } ?>
</div>
</td>
</tr>

</table><br /><br />
</div>
<br />
<?php if($showMoreListings!="no"){ ?>
<h3 class='reHeading1'><?php print $relanguage_tags["Similar listings based on your search criteria"];?></h3>
<div id='reResults2'></div>
<?php } 
return ob_get_clean();
}
?>